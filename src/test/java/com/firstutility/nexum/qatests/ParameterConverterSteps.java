package com.firstutility.nexum.qatests;

import net.serenitybdd.jbehave.SerenityStory;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

public class ParameterConverterSteps extends SerenityStory {
    @Override
    public Configuration configuration() {
        return super.configuration().usePendingStepStrategy(new FailingUponPendingStep());
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new com.firstutility.nexum.qatests.storysteps.ParameterConverterSteps());
    }

    @Override
    protected String getStoryPath() {
        return "**/customer_route.story";
    }
}
