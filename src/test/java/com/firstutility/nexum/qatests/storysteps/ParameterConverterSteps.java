package com.firstutility.nexum.qatests.storysteps;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;

import java.util.List;

public class ParameterConverterSteps {

    @Given("init <ids>")
    public void initIds(@Named("ids") final List<Customer> customers) {
        System.out.println(customers);
    }

    class Customer {
        private String id;

        Customer(String id) {
            this.id = id;
        }

        @Override
        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode(this);
        }

        @Override
        public boolean equals(Object obj) {
            return EqualsBuilder.reflectionEquals(this, obj);
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }

    @AsParameterConverter
    public Customer customer(final String id) {
        return new Customer(id);
    }
}
